angular.module( 'session', ['ngCookies'] )
    .service('AuthService', function($rootScope, $cookieStore, $log, $location) {
        return {
            restore: function() {
                var profile = $cookieStore.get("profile");
                if (typeof profile === 'object')
                {
                    $rootScope.profile = profile;
                    $rootScope.hasAuthorization = true;
                }
            },
            login: function(username, password) {
                if (username === 'admin@admin.com' && password === 'admin')
                {
                    $rootScope.hasAuthorization = true;
                    $rootScope.profile.user = {
                        username: username,
                        name: 'Christopher Kennedy',
                        createdOn: new Date()
                    };
                    $rootScope.profile.token = {
                        public: Math.random().toString(36).substring(7),
                        private: Math.random().toString(36).substring(7)
                    };
                    $cookieStore.put("profile", $rootScope.profile);
                    return true;
                }
                return false;
            },
            destroy: function() {
                $rootScope.hasAuthorization = false;
                $rootScope.profile = {
                    user: {},
                    application: {},
                    token: {}
                };
                $cookieStore.remove("profile");
            }
        };
    })
    .service('FilterService', function($rootScope, $cookieStore, $log, $location) {
      return {
        restore: function() {
          var filter = $cookieStore.get("filter");
          if (typeof filter === 'object')
          {
            $rootScope.filter = filter;
          }
          // Default dates
          else {
            this.update(new Date(), new Date());
          }
        },
        update: function(range_start, range_end) {
          $rootScope.filter = {
            'range_start': range_start,
            'range_end': range_end
          };
          $cookieStore.put("filter", $rootScope.filter);
        }
      };
    })
;
