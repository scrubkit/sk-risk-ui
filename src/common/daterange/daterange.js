angular.module( 'daterange', ['ui.bootstrap', 'ui.router', 'session'] )
    .directive('dateRangeDirective', function($document) {
        return {
            restrict: 'A',
            templateUrl: 'daterange/daterange.tpl.html',
            controllerAs: "DateRangeDirectiveCtrl",
            controller: function($scope, $rootScope, $state, $filter, FilterService) {
                $scope.daterange = 'custom';
                $scope.mouseover = false;
                $scope.today = new Date();
              // Objects for datepickers
                $scope.start =  $rootScope.filter.range_start;
                $scope.end =  $rootScope.filter.range_end;
              // Objects for input fields
                $scope.start_display = $filter('date')($scope.start, "M/d/yyyy");
                $scope.end_display = $filter('date')($scope.end, "M/d/yyyy");
              // State of form
                $scope.showDateSelection = false;

              // Functions
                $scope.cancel = function () {
                  $scope.start =  $rootScope.filter.range_start;
                  $scope.end =  $rootScope.filter.range_end;
                };
                $scope.updateDaterangeReverse = function() {
                  var range;
                  var start = new Date($scope.start );
                  var end = new Date($scope.end );
                  start.setHours(0, 0, 0, 0);
                  end.setHours(0, 0, 0, 0);

                  // Check dates against today's filter
                  range = $scope.today();
                  if (range.start == start && range.end == end) {
                    $scope.daterange = 'today';
                    return;
                  }
                  // Check dates against yesterday's filter
                  range = $scope.yesterday();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'yesterday';
                    return;
                  }
                  // Check dates against this week's filter
                  range = $scope.thisWeek();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'this week';
                    return;
                  }
                  // Check dates against this month's filter
                  range = $scope.thisMonth();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'this month';
                    return;
                  }
                  // Check dates against last week's filter
                  range = $scope.lastWeek();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'last week';
                    return;
                  }
                  // Check dates against last month's filter
                  range = $scope.lastMonth();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'last month';
                    return;
                  }
                  // Check dates against last month's filter
                  range = $scope.lastSevenDays();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'last 7 days';
                    return;
                  }
                  // Check dates against last month's filter
                  range = $scope.lastThirtyDays();
                  if (range.start.toString() == start.toString() && range.end.toString() == end.toString()) {
                    $scope.daterange = 'last 30 days';
                    return;
                  }
                  $scope.daterange = 'custom';
                };

                $scope.updateDaterange = function() {
                  var range = false;

                  switch($scope.daterange) {
                    case 'today':
                      range = $scope.today();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'yesterday':
                      range = $scope.yesterday();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'this week':
                      range = $scope.thisWeek();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'this month':
                      range = $scope.thisMonth();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'last week':
                      range = $scope.lastWeek();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'last month':
                      range = $scope.lastMonth();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'last 7 days':
                      range = $scope.lastSevenDays();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                    case 'last 30 days':
                      range = $scope.lastThirtyDays();
                      $scope.start = range.start;
                      $scope.end = range.end;
                      break;
                  }
                };
                $scope.today = function() {
                  var now = new Date().setHours(0, 0, 0, 0);
                  return {
                    start: now,
                    end: now
                  };
                };
                $scope.yesterday = function() {
                  var now = new Date();
                  now.setDate(now.getDate()-1);
                  now.setHours(0, 0, 0, 0);
                  return {
                    start: now,
                    end: now
                  };
                };
                $scope.thisWeek = function() {
                  var now = new Date();
                  // set to Monday of this week
                  now.setDate(now.getDate() - (now.getDay() + 6) % 7);
                  var range = {
                    start: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                    end: new Date(now.getFullYear(), now.getMonth(), now.getDate()+6)
                  };
                  range.start.setHours(0, 0, 0, 0);
                  range.end.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.thisMonth = function() {
                  var range = {
                    start: false,
                    end: new Date()
                  };
                  range.start = new Date(range.end.getFullYear(), range.end.getMonth(), 1);
                  range.start.setHours(0, 0, 0, 0);
                  range.end.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.lastWeek = function() {
                  var range = {};
                  var now = new Date();
                  // set to Monday of this week
                  now.setDate(now.getDate() - (now.getDay() + 6) % 7);
                  range.end = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
                  range.end.setHours(0, 0, 0, 0);
                  // set to previous Monday
                  now.setDate(now.getDate() - 7);
                  // create new date of day before
                  range.start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
                  range.start.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.lastMonth = function() {
                  var range = {};
                  var now = new Date();
                  if (now.getMonth() === 0) {
                    range.start = new Date(now.getFullYear() - 1, 11, 1);
                  } else {
                    range.start = new Date(now.getFullYear(), now.getMonth() - 1, 1);
                  }
                  range.end = new Date(now.getFullYear(), now.getMonth(), 1);
                  range.end.setDate(range.end.getDate()-1); // - 30 days
                  range.end.setHours(0, 0, 0, 0);
                  range.start.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.lastSevenDays = function() {
                  var range = {};
                  var now = new Date();
                  now.setDate(now.getDate()-6); // - 30 days
                  now.setHours(0, 0, 0, 0);
                  range.start = now;
                  range.end = new Date();
                  range.end.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.lastThirtyDays = function() {
                  var range = {};
                  var now = new Date();
                  now.setDate(now.getDate()-29); // - 30 days
                  now.setHours(0, 0, 0, 0);
                  range.start = now;
                  range.end = new Date();
                  range.end.setHours(0, 0, 0, 0);
                  return range;
                };
                $scope.apply = function () {
                  FilterService.update($scope.start, $scope.end);
                  $scope.toggleDisplay();
                  $state.go($state.$current, null, { reload: true });
                };
                $scope.reset = function () {
                  $scope.start = $rootScope.filter.range_start;
                  $scope.end = $rootScope.filter.range_end;
                };
                $scope.blurred = function(field) {
                  $scope.syncDates(field);
                };
                $scope.open = function($event) {
                  $event.preventDefault();
                  $event.stopPropagation();
                  $scope.opened = true;
                };
                $scope.$watch('start', function() {
                  $scope.start_display = $filter('date')( $scope.start, "M/d/yyyy"  );
                  if ($scope.start > $scope.end) {
                    $scope.end = $scope.start;
                  }
                  $scope.updateDaterangeReverse();
                });
                $scope.$watch('end', function() {
                  $scope.end_display = $filter('date')( $scope.end, "M/d/yyyy"  );
                  $scope.updateDaterangeReverse();
                });
                $scope.syncDates = function(param) {
                  var date = Date.parse($scope[param + "_display"]);
                  if (!isNaN(date)) {
                    $scope[param] = date;
                  }
                };
                $scope.toggleDisplay = function () {
                  $scope.showDateSelection = ($scope.showDateSelection) ? false : true;
                  if ($scope.showDateSelection === true) {
                    $scope.mouseover = null;
                  }
                };

            },
            link: function(scope, element, attrs) {
              element.on("mouseout", function(e) {
                if (scope.showDateSelection === true) {
                  scope.mouseover = false;
                }
              });
              element.on("mouseover", function(e) {
                if (scope.showDateSelection === true) {
                  scope.mouseover = true;
                }
              });
              // Bind escape key to destroy the date range form
              $document.bind("keyup", function(event) {
                if (event.which === 27 && scope.showDateSelection === true) {
                  scope.toggleDisplay();
                  scope.$apply();
                }
              });
              $document.on("click", function(event) {
                if (scope.showDateSelection === true && scope.mouseover === false) {
                  scope.toggleDisplay();
                  scope.$apply();
                }
              });

            }
        };
    })
;
