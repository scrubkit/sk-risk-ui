angular.module( 'api', [] )
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    })
    .factory('authInterceptor', function ($q, $rootScope, $window, $log) {
        return {
            request: function (config) {
                if ($rootScope.hasAuthorization) {
                    if (angular.isDefined($rootScope.profile.token.public)) {
                        //config.headers.common.Authorization = 'Basic ' + $rootScope.profile.token.public;
                        config.headers.Authorization = 'Basic ' + $rootScope.profile.token.public;
                    }
                }
                return config || $q.when(config);
                /*
                config.headers = config.headers || {};
                if ($rootScope.profile.token.public) {
                    //$log.info($rootScope.profile.token.public);
                    config.headers.Authorization = 'Bearer ' + $rootScope.profile.token.public;
                }
                return config;
                */
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    // handle the case where the user is not authenticated
                }
                return $q.reject(rejection);
            }
        };
    })
    .service('RequestService', function($rootScope, $http) {
        var filter = $rootScope.filter;
        return {
            api: "//api.zxcvtrack.mbp",
            fetch: function() {
                    $http({
                      url: this.api + "/internal",
                      method: 'GET',
                      params: {
                        filter: filter
                      }
                    })
                    .success(function(data, status, headers, config){
                        console.log(data);
                    })
                    .error(function (data, status, headers, config) {
                    });
                /*
                 $http
                 .post('/authenticate', $scope.user)
                 .success(function (data, status, headers, config) {
                 $window.sessionStorage.token = data.token;
                 $scope.isAuthenticated = true;
                 var encodedProfile = data.token.split('.')[1];
                 var profile = JSON.parse(url_base64_decode(encodedProfile));
                 $scope.welcome = 'Welcome ' + profile.first_name + ' ' + profile.last_name;
                 })
                 .error(function (data, status, headers, config) {
                 // Erase the token if the user fails to log in
                 delete $window.sessionStorage.token;
                 $scope.isAuthenticated = false;

                 // Handle login errors here
                 $scope.error = 'Error: Invalid user or password';
                 $scope.welcome = '';
                 });
                 */
            },
            parse: function() {

            }
        };
    })
;
