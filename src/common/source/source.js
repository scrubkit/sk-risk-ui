angular.module( 'source', ['ui.bootstrap', 'ui.router', 'session'] )
  .directive('sourceDirective', function($document) {
    return {
      restrict: 'A',
      templateUrl: 'source/source.tpl.html',
      controllerAs: "SourceDirectiveCtrl",
      controller: function ($scope, $rootScope, $state, $filter, FilterService) {
        $scope.showingSources = false;
        $scope.mouseover = false;
        $scope.toggleSources = function() {
          $scope.showingSources = ($scope.showingSources) ? false : true;
          if ($scope.showingSources === true) {
            $scope.mouseover = null;
          }
        };
      },
      link: function(scope, element, attrs) {
        element.on("mouseout", function(e) {
          if (scope.showingSources === true) {
            scope.mouseover = false;
          }
        });
        element.on("mouseover", function(e) {
          if (scope.showingSources === true) {
            scope.mouseover = true;
          }
        });
        // Bind escape key to destroy the date range form
        $document.bind("keyup", function(event) {
           if (event.which === 27 && scope.showingSources === true) {
           scope.toggleSources();
           scope.$apply();
           }
        });
        $document.on("click", function(event) {
          if (scope.showingSources === true && scope.mouseover === false) {
            scope.toggleSources();
            scope.$apply();
          }
        });

      }
    };
  });