angular.module( 'search', ['ui.bootstrap', 'ui.router', 'session'] )
  .directive('searchDirective', function($document) {
    return {
      restrict: 'A',
      templateUrl: 'search/search.tpl.html',
      controllerAs: "SearchDirectiveCtrl",
      controller: function ($scope, $rootScope, $state, $filter, FilterService) {
        $scope.showingSearchResults = false;
        $scope.mouseover = false;
        $scope.timeout = null;
        $scope.query = '';
        $scope.results = '';
        $scope.queryServer = function() {
          if ($scope.timeout !== null) {
            clearTimeout($scope.timeout);
          }
          $scope.timeout = setTimeout(function(){
            console.log("Search query for .. " + $scope.query);
            /*
             $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
             params: {
             address: val,
             sensor: false
             }
             }).then(function(response){
             return response.data.results.map(function(item){
             return item.formatted_address;
             });
             });
             */

          }, 300);
        };
        $scope.toggleSearchResults = function() {
          $scope.showingSearchResults = ($scope.showingSearchResults) ? false : true;
          if ($scope.showingSearchResults === true) {
            $scope.mouseover = null;
          }
        };
        $scope.showSearchResults = function() {
          if ($scope.showingSearchResults === false) {
            $scope.showingSearchResults = true;
          }
          //$scope.mouseover = null;
        };
        $scope.hideSearchResults = function() {
          if ($scope.showingSearchResults === false) {
            $scope.showingSearchResults = true;
            $scope.mouseover = null;
          }
        };
      },
      link: function(scope, element, attrs) {
        element.on("mouseout", function(e) {
          if (scope.showingSearchResults === true) {
            scope.mouseover = false;
          }
        });
        element.on("mouseover", function(e) {
          if (scope.showingSearchResults === true) {
            scope.mouseover = true;
          }
        });
        // Bind escape key to destroy the date range form
        /*
        $document.bind("keyup", function(event) {
          if (event.which === 27 && scope.showingSearchResults === true) {
            scope.toggleSearchResults();
            scope.$apply();
          }
        });
        */
        $document.on("click", function(event) {
          if (scope.showingSearchResults === true && scope.mouseover === false) {
            scope.toggleSearchResults();
            scope.$apply();
          }
        });

      }
    };
  });