angular.module('ngBoilerplate.auth', [
    'ui.router',
    'ui.bootstrap',
    'session'
])

    .config(function config( $stateProvider ) {
        $stateProvider.
            state( 'auth', {
                url: '/auth',
                views: {
                    "main": {
                        controller: function($rootScope, $location) { },
                        templateUrl: 'auth/auth.tpl.html'
                    }
                },
                data:{ pageTitle: 'Login', redirectAuthorized: true }
            })
            .state( 'auth.login', {
                url: '.login',
                views: {
                    "auth": {
                        controller: 'LoginCtrl',
                        templateUrl: 'auth/login.tpl.html'
                    }
                },
                data:{ pageTitle: 'Login', skipAuthorization: true }
            })
            .state( 'auth.logout', {
                url: '.logout',
                views: {
                    "auth": {
                        controller: 'LogoutCtrl',
                        templateUrl: 'auth/logout.tpl.html'
                    }
                },
                data:{ pageTitle: 'Logout' }
            })
            .state( 'auth.password', {
                url: '.password',
                views: {
                    "auth": {
                        controller: 'PasswordCtrl',
                        templateUrl: 'auth/password.tpl.html'
                    }
                },
                data:{ pageTitle: 'Password Reset', skipAuthorization: true, redirectAuthorized: true }
            })
        ;
    })

    .controller( 'LoginCtrl', function ( $scope, $location, $rootScope, AuthService ) {
        $scope.user = '';
        $scope.password = '';
        $scope.error = '';

        $scope.authenticate = function() {
            if (AuthService.login($scope.user.email, $scope.user.password))
            {
                $scope.error = '';
                $location.path("/dashboard");
                $scope.user.password = '';
            }
            else
            {
                //$scope.
                $scope.error = "We could not signed you into your account";
            }
        };
        $scope.reset();
    })

    .controller( 'LogoutCtrl', function ( $scope, $location, AuthService ) {
        AuthService.destroy();
        $location.path("/auth.login");
    })

    .controller( 'PasswordCtrl', function ( $scope, $location, AuthService ) {

    })
;
