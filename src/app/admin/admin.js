angular.module('ngBoilerplate.admin', [
    'ui.router',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'admin', {
            url: '/admin',
            views: {
                "main": {
                    controller: 'AdminCtrl',
                    templateUrl: 'admin/admin.tpl.html'
                }
            },
            data:{ pageTitle: 'Admin' }
        });
    })

    .controller( 'AdminCtrl', function ( $scope ) {
        // This is simple a demo for UI Boostrap.

    })
;
