angular.module( 'ngBoilerplate.report', [
    'ui.router'
])

.config(function config( $stateProvider ) {
    $stateProvider.
        state( 'report', {
            url: '/report',
            views: {
                "main": {
                    controller: 'ReportCtrl',
                    templateUrl: 'report/report.tpl.html'
                }
            },
            data:{ pageTitle: 'Report' }
        })
        .state("report.overview", {
            url: ".overview",
            views: {
                "report": {
                    controller: 'ReportOverviewCtrl',
                    templateUrl: 'report/report.overview.tpl.html'
                }
            },
            data:{ pageTitle: 'Report Overview' }
        })
        .state("report.geography", {
            url: ".geography",
            views: {
                "report": {
                    controller: 'ReportGeographyCtrl',
                    templateUrl: 'report/report.geography.tpl.html'
                }
            },
            data:{ pageTitle: 'Report Geography' }
        });
})

    .controller('ReportCtrl', function ( $scope ) {
        // This is simple a demo for UI Boostrap.

    })

    .controller('ReportOverviewCtrl', function ( $scope ) {
        // This is simple a demo for UI Boostrap.

    })

    .controller('ReportGeographyCtrl', function ( $scope ) {
        // This is simple a demo for UI Boostrap.

    })

    .directive('reportPrimaryChart', function () {
        return {
            restrict: 'A',
            templateUrl: 'report/report.graph.tpl.html'
        };
    })
;
