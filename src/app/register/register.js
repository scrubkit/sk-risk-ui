angular.module( 'ngBoilerplate.register', [
    'ui.router',
    'ui.bootstrap'
])
.config(function config( $stateProvider ) {
    $stateProvider.state( 'register', {
        url: '/register',
        views: {
            "main": {
                controller: 'RegisterCtrl',
                templateUrl: 'register/register.tpl.html'
            }
        },
        data:{ pageTitle: 'Register', skipAuthorization: true, redirectAuthorized: true }
    });
})

.controller( 'RegisterCtrl', function ( $scope ) {
    $scope.partners = [
      { name: 'Cake' },
      { name: 'HasOffers by Tune' },
      { name: 'Mobile App Tracking by Tune' },
      { name: 'LinkTrust' },
      { name: 'Other' }
    ];
    $scope.countries = [
      { name: 'Canada', code: 'CA' },
      { name: 'United States', code: 'US' },
      { name: 'United Kingdom', code: 'UK' }
    ];
    $scope.website_types = [
      { name: 'Lead Generation' },
      { name: 'Membership Site' },
      { name: 'E-Commerce' },
      { name: 'Other' }
    ];

    $scope.tracking = {
      platform: 'Cake'
    };
})
.directive('passwordConfirm', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    scope: {
      matchTarget: '='
    },
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      var validator = function (value) {
        ctrl.$setValidity('match', value === scope.matchTarget);
        return value;
      };

      ctrl.$parsers.unshift(validator);
      ctrl.$formatters.push(validator);

      // This is to force validator when the original password gets changed
      scope.$watch('matchTarget', function(newval, oldval) {
        validator(ctrl.$viewValue);
      });

    }
  };
}])
;
