angular.module('ngBoilerplate', [
    'templates-app',
    'templates-common',
    'ngBoilerplate.register',
    'ngBoilerplate.auth',
    'ngBoilerplate.home',
    'ngBoilerplate.admin',
    'ngBoilerplate.dashboard',
    'ngBoilerplate.report',
    'ui.router',
    'setting',
    'session',
    'daterange',
    'search',
    'source',
    'api'
])

    .config(function myAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard');
    })

    .run(function run($rootScope, AuthService, FilterService, $log) {
        $rootScope.hasAuthorization = false;
        $rootScope.company = {
            name: "ScrubKit Corp",
            copyright: "2014",
            created: new Date(2011, 1, 15),
            phone: "(800) 838-4713",
            fax: "(800) 838-4713",
            email: "support@scrubkit.com",
            website: "http://www.scrubkit.com"
        };
        $rootScope.source = {
            id: "1",
            name: "Scrubkit",
            property: {
              id: '1-1',
              name: 'Demo Account'
            }
        };
        $rootScope.filter = {};
        $rootScope.profile = {};
        AuthService.restore();
        FilterService.restore();
    })

    .controller('AppCtrl', function AppCtrl($scope, $rootScope, $location, $log) {
        $scope.company = $rootScope.company;
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | ' + $rootScope.company.name;
            } else {
                $scope.pageTitle = $rootScope.company.name;
            }
            /**
             * Force authorization on user if content has not been set to skipAuthorization
             */
            var skipAuth = (angular.isDefined(toState.data.skipAuthorization)) ? toState.data.skipAuthorization : false;
            if (skipAuth === false && $rootScope.hasAuthorization === false) {
                $location.path("/auth.login");
            }
            /**
             * Redirect users that have already been authorized if pages are intended for unauthorized eyes
             */
            var redirectAuthorized = (angular.isDefined(toState.data.redirectAuthorized)) ? toState.data.redirectAuthorized : false;
            if (redirectAuthorized === true && $rootScope.hasAuthorization === true) {
                $location.path("/dashboard");
            }
        });
    });

