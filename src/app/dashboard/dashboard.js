angular.module( 'ngBoilerplate.dashboard', [
    'ui.router'
])

.config(function config( $stateProvider ) {
    $stateProvider.state( 'dashboard', {
        url: '/dashboard',
        views: {
            "main": {
                controller: 'DashboardCtrl',
                controllerAs: 'dashboard',
                templateUrl: 'dashboard/dashboard.tpl.html'
            }
        },
        data:{ pageTitle: 'Dashboard' }
    });
})

.controller( 'DashboardCtrl', function ( $scope, $rootScope ) {
    // This is simple a demo for UI Boostrap.
    this.data = [{
        id: 1,
        name: 'ScrubKit',
        property: [{
            id: '1-1',
            name: 'Demo Account',
            views: 0,
            sessions: 0,
            conversions: 0,
            duplicitous: 0,
            sales: 1245.00,
            estimated_loss: 192,
            threats: ['abusive IP usage','dupe IPs','excessive server']
        },{
            id: '1-1',
            name: 'Demo Account 2',
            views: 0,
            sessions: 0,
            conversions: 0,
            duplicitous: 0,
            sales: 1245,
            estimated_loss: 192,
            threats: ['abusive IP usage','dupe IPs','excessive server']
        }]
    },{
        id: 2,
        name: 'EnvyusMedia',
        property: [{
            id: '2-1',
            name: 'Cake Marketing',
            views: 53612,
            sessions: 13403,
            conversions: 4124,
            duplicitous: 123,
            sales: 345,
            estimated_loss: 93,
            threats: ['abusive IP usage','dupe IPs']
        },{
            id: '2-2',
            name: 'Insurance Leads',
            views: 0,
            sessions: 0,
            conversions: 0,
            duplicitous: 0,
            sales: 3233,
            estimated_loss: 342,
            threats: ['excessive organization']
        }]
    },{
        id: 2,
        name: 'Some Other Acmee',
        property: [{
            id: '3-1',
            name: 'Cake Marketing',
            views: 53612,
            sessions: 13403,
            conversions: 4124,
            duplicitous: 123,
            sales: 345,
            estimated_loss: 93,
            threats: ['abusive IP usage','dupe IPs']
        },{
            id: '3-2',
            name: 'HasOffers',
            views: 0,
            sessions: 0,
            conversions: 0,
            duplicitous: 0,
            sales: 3234,
            estimated_loss: 342,
            threats: ['excessive organization']
        }]
    }];
})
;
